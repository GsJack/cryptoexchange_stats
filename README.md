# cryptoexchange_stats

Due to limitations of [basecamp API](https://github.com/basecamp/bc3-api), the script is only able to get following data:
- Total **Completed Tickets**
- Total **Active Tickets**

PS: the script is not able to get the **completed tickets** and **active tickets** within a **date range**, data generated is as of the time of running the script.

## Getting started

`rb get_stats.rb`

```
Enter client_id:

Enter client_secret:

Enter account_number:

please go to below url to get authorization_token (check the url for code)

https://launchpad.37signals.com/authorization/new?client_id=fcf90a625d14958819012d91c4a07d24c0d97fda&redirect_uri=https%3A%2F%2F5d32-2001-f40-90f-89b-10bd-8dc4-59ac-6b84.ngrok.io%2Fbadurlonpurpose&response_type=code&type=web_server

Enter authorization_token:
76198efb

DateTime: 2021-10-14 16:44:04 +0800
Total
======
Active
17
Completed
109
```

**client_id**
obtain from [1Password](https://coingecko.1password.com/vaults/zmgyfuq23wwvn7k5wzmq5lvxjq/allitems/wuug2bvbta4rqvsh6zmrfvrt2i)

**client_secret**
obtain from [1Password](https://coingecko.1password.com/vaults/zmgyfuq23wwvn7k5wzmq5lvxjq/allitems/wuug2bvbta4rqvsh6zmrfvrt2i)

**account_number**
get the value from [Basecamp Sign in](https://launchpad.37signals.com/signin), once signed in, you will be redirected to your account dashboard for example https://3.basecamp.com/4235100/
Hence in this case **4235100** is the value you are looking for.


## License

Copyright 2021 @GsJack

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

