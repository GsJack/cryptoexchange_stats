# gem install oauth2
# gem install httparty

require 'oauth2'
require "httparty"

puts "Enter client_id:"
client_id = gets.chomp
puts "Enter client_secret:"
client_secret = gets.chomp
puts "Enter account_number:"
account_number = gets.chomp

client = OAuth2::Client.new(
  client_id,
  client_secret,
  site: 'https://5d32-2001-f40-90f-89b-10bd-8dc4-59ac-6b84.ngrok.io',
  authorize_url: "https://launchpad.37signals.com/authorization/new",
  token_url: "https://launchpad.37signals.com/authorization/token"
)

user_interaction_url = client.auth_code.authorize_url(redirect_uri: 'https://5d32-2001-f40-90f-89b-10bd-8dc4-59ac-6b84.ngrok.io/badurlonpurpose', type: "web_server")

puts ""
puts "please go to below url to get authorization_token (check the url parameter 'code')"
puts ""
puts user_interaction_url
puts ""
puts "Enter authorization_token:"
authorization_token = gets.chomp
token = client.auth_code.get_token(authorization_token, type: "web_server", redirect_uri: 'https://5d32-2001-f40-90f-89b-10bd-8dc4-59ac-6b84.ngrok.io/badurlonpurpose')

@access_token = token.token
#refresh_token = token.refresh_token

def get_total_tickets(status)
  url = if status
          if status == "completed"
            "https://3.basecampapi.com/4235100/buckets/14713404/todolists/3473728421/todos.json?completed=true"
          elsif status == "active"
            "https://3.basecampapi.com/4235100/buckets/14713404/todolists/3473728421/todos.json?"
          else
            "https://3.basecampapi.com/4235100/buckets/14713404/todolists/3473728421/todos.json?status=#{status}"
          end
        else
          "https://3.basecampapi.com/4235100/buckets/14713404/todolists/3473728421/todos.json?"
        end

  headers = {
    Authorization: "Bearer #{@access_token}"
  }

  tickets = []
  page = 1
  loop do
    response = HTTParty.get("#{url}&page=#{page}", headers: headers)
    tickets << response
    page += 1
  break if response.count == 0

  end
  tickets.flatten!
  tickets
end

def get_tickets_of_month(tickets, date = Date.current)
  start_dt = date.to_datetime.beginning_of_month
  end_dt = date.to_datetime.end_of_month

  tickets_of_months = tickets.dup
  tickets_of_months = tickets_of_months.delete_if do |ticket|
    ticket_created_at = ticket["created_at"].to_datetime
    ticket_created_at < start_dt || ticket_created_at > end_dt
  end
  tickets_of_months
end

active_tickets = get_total_tickets("active")
# active_tickets_this_month = get_tickets_of_month(active_tickets)
#
completed_tickets = get_total_tickets("completed")
# completed_tickets_this_month = get_tickets_of_month(completed_tickets)
#
# trashed_tickets = get_total_tickets("trashed")
# trashed_tickets_this_month = get_tickets_of_month(trashed_tickets)
#
# archived_tickets = get_total_tickets("archived")
# archived_tickets_this_month = get_tickets_of_month(archived_tickets)

puts ""
puts "DateTime: #{Time.now.strftime("%d/%m/%Y %H:%M:%S")}"
puts "Total"
puts "======"
puts "Active"
puts "#{active_tickets.count}"
puts "Completed"
puts "#{completed_tickets.count}"
